let items = [
	{
		id: 1,
		name: "Razer Black Widow",
		desc: "Gaming keyboard",
		stock: 10,
		isAvailable: true,
		dateAdded: "March 10, 2021"
	},
	{
		id: 2,
		name: "Razer Viper Mini",
		desc: "Gaming mouse",
		stock: 12,
		isAvailable: true,
		dateAdded: "March 1, 2021"
	}
]

const addItem = (id, name, desc, stock, dateAdded) => {
	let newItem = {
		id:id,
		name:name,
		desc:desc,
		stock:stock,
		isAvailable:true,
		dateAdded:dateAdded
	}
	items.push(newItem)
	alert(`You have created ${name}. The stock remaining is ${stock}.`)
}


const getSingleItemStock = (id) => {
	let foundItem = items.find((items) => id == items.id)
		if(id == items.id){
		alert(`${foundItem.name} has ${foundItem.stock} left.`)
		}
		else{
			alert("Item not found.")
		}
}

const archieveItem = (id) =>{
	let foundStock = items.find((items) => id == items.id)
	foundStock.isAvailable = false
}

const deleteItem = () => items.pop()